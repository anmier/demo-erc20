# Substrate Demo-ERC20 Pallet

A basic implementation of ERC-20 standard for Fungible Tokens.

## Dispatchable Functions

* `transfer` - Move some valid amount of tokens to another account.
* `transfer_from` - Move some valid amount of tokens from one account to another using the allowance mechanism.

More details on the known assumptions for the standard can be found
[here](https://eips.ethereum.org/EIPS/eip-20)