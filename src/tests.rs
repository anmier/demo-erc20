use crate::{mock::*, Error};
use frame_support::{assert_noop, assert_ok};

use super::Event as Erc20Event;

pub fn events() -> Vec<Erc20Event<Test>> {
	let ev = System::events()
		.into_iter()
		.map(|evt| evt.event)
		.filter_map(|e| if let Event::DemoErc20(inner) = e { Some(inner) } else { None })
		.collect::<Vec<_>>();
	System::reset_events();

	ev
}

#[test]
fn balance_of_should_return_zero() {
	new_test_ext().execute_with(|| {
		assert_eq!(DemoErc20::balance_of(Origin::signed(1)).unwrap(), 0);
	});
}

#[test]
fn allowance_should_return_zero() {
	new_test_ext().execute_with(|| {
		assert_eq!(DemoErc20::allowance(Origin::signed(1), 2).unwrap(), 0);
	});
}

#[test]
fn force_create_metadata_should_work() {
	new_test_ext().execute_with(|| {
		assert_ok!(DemoErc20::force_create_metadata(
			Origin::root(),
			b"AwesomeName".to_vec(),
			b"SYM".to_vec(),
			18
		));
		assert_eq!(DemoErc20::token_name(), b"AwesomeName");
		assert_eq!(DemoErc20::token_symbol(), b"SYM");
		assert_eq!(DemoErc20::token_decimals(), 18);
	});
}

#[test]
fn force_create_metadata_with_long_string_should_fail() {
	new_test_ext().execute_with(|| {
		assert_noop!(
			DemoErc20::force_create_metadata(
				Origin::root(),
				b"TooLooooooooooooooooooooongName".to_vec(),
				b"SYM".to_vec(),
				18
			),
			Error::<Test>::StringTooLong,
		);
		assert!(DemoErc20::token_name().is_empty());
		assert!(DemoErc20::token_symbol().is_empty());
		assert_eq!(DemoErc20::token_decimals(), 0);
	});
}

#[test]
fn force_set_balance_should_work() {
	new_test_ext().execute_with(|| {
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 1, 100));
		assert_eq!(DemoErc20::balance_of(Origin::signed(1)).unwrap(), 100);
		assert_eq!(events(), [Erc20Event::Transfer { from: None, to: 1, value: 100 },]);
	});
}

#[test]
fn transfer_should_work() {
	new_test_ext().execute_with(|| {
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 1, 100));
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 2, 200));
		assert_ok!(DemoErc20::transfer(Origin::signed(2), 1, 10));

		assert_eq!(DemoErc20::balance_of(Origin::signed(1)).unwrap(), 110);
		assert_eq!(DemoErc20::balance_of(Origin::signed(2)).unwrap(), 190);
		assert_eq!(
			events(),
			[
				Erc20Event::Transfer { from: None, to: 1, value: 100 },
				Erc20Event::Transfer { from: None, to: 2, value: 200 },
				Erc20Event::Transfer { from: Some(2), to: 1, value: 10 },
			]
		);
	});
}

#[test]
fn transfer_with_zero_amount_should_work() {
	new_test_ext().execute_with(|| {
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 1, 100));
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 2, 200));
		assert_ok!(DemoErc20::transfer(Origin::signed(2), 1, 0));

		assert_eq!(DemoErc20::balance_of(Origin::signed(1)).unwrap(), 100);
		assert_eq!(DemoErc20::balance_of(Origin::signed(2)).unwrap(), 200);
		assert_eq!(
			events(),
			[
				Erc20Event::Transfer { from: None, to: 1, value: 100 },
				Erc20Event::Transfer { from: None, to: 2, value: 200 },
				Erc20Event::Transfer { from: Some(2), to: 1, value: 0 },
			]
		);
	});
}

#[test]
fn transfer_for_same_account_should_fail() {
	new_test_ext().execute_with(|| {
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 1, 100));
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 2, 200));
		assert_noop!(
			DemoErc20::transfer(Origin::signed(2), 2, 50),
			Error::<Test>::InvalidTransactor,
		);
		// Verify that both credit and debit balances have not been affected
		assert_eq!(DemoErc20::balance_of(Origin::signed(1)).unwrap(), 100);
		assert_eq!(DemoErc20::balance_of(Origin::signed(2)).unwrap(), 200);
		assert_eq!(
			events(),
			[
				// Transfer events emitted on the balances setup only
				Erc20Event::Transfer { from: None, to: 1, value: 100 },
				Erc20Event::Transfer { from: None, to: 2, value: 200 },
			]
		);
	});
}

#[test]
fn transfer_with_overflow_should_fail() {
	new_test_ext().execute_with(|| {
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 1, u32::MAX));
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 2, u32::MAX));
		assert_noop!(
			DemoErc20::transfer(Origin::signed(2), 1, u32::MAX),
			sp_runtime::ArithmeticError::Overflow
		);
		// Verify that both credit and debit balances have not been affected
		assert_eq!(DemoErc20::balance_of(Origin::signed(2)).unwrap(), u32::MAX);
		assert_eq!(DemoErc20::balance_of(Origin::signed(1)).unwrap(), u32::MAX);
		assert_eq!(
			events(),
			[
				// Transfer events emitted on the balances setup only
				Erc20Event::Transfer { from: None, to: 1, value: u32::MAX },
				Erc20Event::Transfer { from: None, to: 2, value: u32::MAX },
			]
		);
	});
}

#[test]
fn transfer_for_low_balance_should_fail() {
	new_test_ext().execute_with(|| {
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 1, 100));
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 2, 200));
		assert_noop!(DemoErc20::transfer(Origin::signed(1), 2, 300), Error::<Test>::BalanceLow,);
		// Verify that both credit and debit balances have not been affected
		assert_eq!(DemoErc20::balance_of(Origin::signed(1)).unwrap(), 100);
		assert_eq!(DemoErc20::balance_of(Origin::signed(2)).unwrap(), 200);
		assert_eq!(
			events(),
			[
				// Transfer events emitted on the balances setup only
				Erc20Event::Transfer { from: None, to: 1, value: 100 },
				Erc20Event::Transfer { from: None, to: 2, value: 200 },
			]
		);
	});
}

#[test]
fn transfer_from_should_work() {
	new_test_ext().execute_with(|| {
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 1, 100));
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 2, 200));
		assert_ok!(DemoErc20::force_set_allowance(Origin::root(), 2, 1, 50));
		assert_ok!(DemoErc20::transfer_from(Origin::root(), 2, 1, 10));

		assert_eq!(DemoErc20::balance_of(Origin::signed(1)).unwrap(), 110);
		assert_eq!(DemoErc20::balance_of(Origin::signed(2)).unwrap(), 190);
		// Remaining allowance on approval
		let remaining = 50 - 10;
		assert_eq!(DemoErc20::allowance(Origin::signed(2), 1).unwrap(), remaining);
		assert_eq!(
			events(),
			[
				Erc20Event::Transfer { from: None, to: 1, value: 100 },
				Erc20Event::Transfer { from: None, to: 2, value: 200 },
				Erc20Event::Approval { owner: 2, spender: 1, value: remaining },
				Erc20Event::Transfer { from: Some(2), to: 1, value: 10 },
			]
		);
	});
}

#[test]
fn transfer_from_with_zero_amount_should_work() {
	new_test_ext().execute_with(|| {
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 1, 100));
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 2, 200));
		assert_ok!(DemoErc20::transfer_from(Origin::root(), 2, 1, 0));

		assert_eq!(DemoErc20::balance_of(Origin::signed(1)).unwrap(), 100);
		assert_eq!(DemoErc20::balance_of(Origin::signed(2)).unwrap(), 200);
		assert_eq!(
			events(),
			[
				Erc20Event::Transfer { from: None, to: 1, value: 100 },
				Erc20Event::Transfer { from: None, to: 2, value: 200 },
				Erc20Event::Approval { owner: 2, spender: 1, value: 0 },
				Erc20Event::Transfer { from: Some(2), to: 1, value: 0 },
			]
		);
	});
}

#[test]
fn transfer_from_with_insufficient_allowance_should_fail() {
	new_test_ext().execute_with(|| {
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 1, 100));
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 2, 200));
		assert_ok!(DemoErc20::force_set_allowance(Origin::root(), 2, 1, 50));
		assert_noop!(
			DemoErc20::transfer_from(Origin::root(), 2, 1, 60),
			Error::<Test>::InsufficientAllowance,
		);
		// Verify that credit and debit balances and allowance have not been affected
		assert_eq!(DemoErc20::balance_of(Origin::signed(1)).unwrap(), 100);
		assert_eq!(DemoErc20::balance_of(Origin::signed(2)).unwrap(), 200);
		assert_eq!(DemoErc20::allowance(Origin::signed(2), 1).unwrap(), 50);
		assert_eq!(
			events(),
			[
				// Transfer events emitted on the balances setup only
				Erc20Event::Transfer { from: None, to: 1, value: 100 },
				Erc20Event::Transfer { from: None, to: 2, value: 200 },
			]
		);
	});
}
#[test]
fn transfer_from_for_same_account_should_fail() {
	new_test_ext().execute_with(|| {
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 1, 100));
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 2, 200));
		assert_noop!(
			DemoErc20::transfer_from(Origin::root(), 2, 2, 10),
			Error::<Test>::InvalidTransactor,
		);
		// Verify that credit and debit balances have not been affected
		assert_eq!(DemoErc20::balance_of(Origin::signed(1)).unwrap(), 100);
		assert_eq!(DemoErc20::balance_of(Origin::signed(2)).unwrap(), 200);
		assert_eq!(
			events(),
			[
				// Transfer events emitted on the balances setup only
				Erc20Event::Transfer { from: None, to: 1, value: 100 },
				Erc20Event::Transfer { from: None, to: 2, value: 200 },
			]
		);
	});
}

#[test]
fn transfer_from_for_low_balance_should_fail() {
	new_test_ext().execute_with(|| {
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 1, 100));
		assert_ok!(DemoErc20::force_set_balance(Origin::root(), 2, 200));
		assert_ok!(DemoErc20::force_set_allowance(Origin::root(), 2, 1, 250));
		assert_noop!(
			DemoErc20::transfer_from(Origin::root(), 2, 1, 250),
			Error::<Test>::BalanceLow,
		);
		// Verify that credit and debit balances have not been affected
		assert_eq!(DemoErc20::balance_of(Origin::signed(1)).unwrap(), 100);
		assert_eq!(DemoErc20::balance_of(Origin::signed(2)).unwrap(), 200);
		assert_eq!(DemoErc20::allowance(Origin::signed(2), 1).unwrap(), 250);
		assert_eq!(
			events(),
			[
				// Transfer events emitted on the balances setup only
				Erc20Event::Transfer { from: None, to: 1, value: 100 },
				Erc20Event::Transfer { from: None, to: 2, value: 200 },
			]
		);
	});
}
