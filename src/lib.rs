#![cfg_attr(not(feature = "std"), no_std)]

pub use pallet::*;

#[cfg(test)]
mod mock;

#[cfg(test)]
mod tests;

#[frame_support::pallet]
pub mod pallet {
	use frame_support::pallet_prelude::*;
	use frame_system::pallet_prelude::*;

	use codec::{Codec, Decode, Encode, MaxEncodedLen};
	use frame_support::transactional;
	use scale_info::TypeInfo;
	use sp_runtime::{
		traits::{
			AtLeast32BitUnsigned, Bounded, CheckedAdd, CheckedSub, MaybeSerializeDeserialize,
			StaticLookup, Zero,
		},
		ArithmeticError, DispatchError,
	};

	/// Metadata for the token: name, symbol and decimals.
	///
	/// The number of decimals used to get its user representation.
	/// For example, if `decimals` equals `2`, a balance of `505` tokens should
	/// be displayed to a user as `5.05` (`505 / 10 ** 2`).
	#[derive(Clone, Encode, Decode, Eq, PartialEq, Default, MaxEncodedLen, TypeInfo)]
	pub struct TokenMetadata<BoundedString> {
		///  The name of the token. Limited in length by `StringLimit`.
		pub(super) name: BoundedString,
		/// The symbol of the token. Limited in length by `StringLimit`.
		pub(super) symbol: BoundedString,
		/// The number of decimals this token uses to represent one unit.
		pub(super) decimals: u8,
	}

	#[pallet::config]
	pub trait Config: frame_system::Config {
		/// The balance of an account.
		type Balance: Parameter
			+ Member
			+ AtLeast32BitUnsigned
			+ Codec
			+ Default
			+ Copy
			+ MaybeSerializeDeserialize
			+ MaxEncodedLen
			+ TypeInfo;

		/// The origin which may forcibly alter privileged attributes.
		type ForceOrigin: EnsureOrigin<Self::Origin>;

		/// The maximum length of a name or symbol stored on-chain.
		#[pallet::constant]
		type StringLimit: Get<u32>;

		/// The overarching event type.
		type Event: From<Event<Self>> + IsType<<Self as frame_system::Config>::Event>;
	}

	#[pallet::pallet]
	#[pallet::generate_store(pub(super) trait Store)]
	pub struct Pallet<T>(_);

	#[pallet::storage]
	/// Metadata for the token: name, symbol and decimals.
	pub type Metadata<T: Config> =
		StorageValue<_, TokenMetadata<BoundedVec<u8, T::StringLimit>>, ValueQuery>;

	#[pallet::storage]
	#[pallet::getter(fn total_supply)]
	/// The total token supply
	pub type TotalSupply<T: Config> = StorageValue<_, T::Balance>;

	#[pallet::storage]
	/// Account balances
	pub(super) type Balances<T: Config> =
		StorageMap<_, Blake2_128Concat, T::AccountId, T::Balance, OptionQuery>;

	#[pallet::storage]
	/// The number of tokens that `spender` will be allowed to spend on behalf of `owner`
	/// through `transfer_from`.
	pub(super) type Allowances<T: Config> = StorageDoubleMap<
		_,
		Blake2_128Concat,
		T::AccountId,
		Blake2_128Concat,
		T::AccountId,
		T::Balance,
		OptionQuery,
		GetDefault,
		ConstU32<300_000>,
	>;

	#[pallet::event]
	#[pallet::generate_deposit(pub(super) fn deposit_event)]
	pub enum Event<T: Config> {
		/// Emitted when `value` tokens are moved from one account to another
		Transfer { from: Option<T::AccountId>, to: T::AccountId, value: T::Balance },
		/// Emitted when the allowance of a `spender` for an `owner` is set by a call to `approve`
		Approval { owner: T::AccountId, spender: T::AccountId, value: T::Balance },
	}

	#[pallet::error]
	pub enum Error<T> {
		/// No approval exists that would allow the transfer.
		Unapproved,
		/// Transfer attempted for the same account balance (`from` is equal to `to`).
		InvalidTransactor,
		/// Account balance must be greater than or equal to the transfer amount.
		BalanceLow,
		/// Not enough allowance to fulfill a request is available.
		InsufficientAllowance,
		/// The signing account has no permission to do the operation.
		NoPermission,
		/// Metadata token strings exceed the length  limit.
		StringTooLong,
	}

	#[pallet::call]
	impl<T: Config> Pallet<T> {
		/// Moves `amount` tokens from the sender (`origin`) account to another.
		///
		/// The `origin` must be signed.
		///
		/// The caller’s account balance must have enough tokens to spend.
		///
		/// - `origin`: The account to be debited.
		/// - `dest`: The account to be credited.
		/// - `amount`: The amount by which the sender's balance of assets should be reduced and
		/// `dest`'s balance increased.
		///
		/// Emits `Transfer` with the actual amount transferred.
		///
		/// Weight is an arbitrary value.
		#[pallet::weight(100)]
		pub fn transfer(
			origin: OriginFor<T>,
			dest: <T::Lookup as StaticLookup>::Source,
			#[pallet::compact] amount: T::Balance,
		) -> DispatchResult {
			let from = ensure_signed(origin)?;
			let to = T::Lookup::lookup(dest)?;

			Self::do_transfer_from_to(&from, &to, amount).map(|_| ())
		}

		/// Moves `amount` tokens from `source` to `dest` using the allowance mechanism.
		/// `amount` is then deducted from the caller's allowance.
		/// Used for a withdraw workflow, allowing contracts to transfer tokens on
		/// somebody's behalf.
		///
		/// The `origin` must conform to `ForceOrigin`.
		///
		/// The source account balance must have enough tokens to spend.
		///
		///
		/// - `source`: The account to be debited.
		/// - `dest`: The account to be credited.
		/// - `amount`: The amount by which the sender's balance of assets should be reduced and
		/// `dest`'s balance increased.
		///
		/// Emits `Transfer` with the actual amount transferred.
		///
		/// Weight is an arbitrary value.
		#[transactional]
		#[pallet::weight(200)]
		pub fn transfer_from(
			origin: OriginFor<T>,
			source: <T::Lookup as StaticLookup>::Source,
			dest: <T::Lookup as StaticLookup>::Source,
			#[pallet::compact] amount: T::Balance,
		) -> DispatchResult {
			T::ForceOrigin::ensure_origin(origin)?;
			let from = T::Lookup::lookup(source)?;
			let to = T::Lookup::lookup(dest)?;

			Self::spend_allowance(&from, &to, amount)?;
			Self::do_transfer_from_to(&from, &to, amount).map(|_| ())
		}
	}

	impl<T: Config> Pallet<T> {
		/// Returns the name of the token.
		pub(super) fn token_name() -> Vec<u8> {
			<Metadata<T>>::get().name.to_vec()
		}

		/// Returns the symbol of the token.
		pub(super) fn token_symbol() -> Vec<u8> {
			<Metadata<T>>::get().symbol.to_vec()
		}

		/// Returns the decimals of the token.
		pub(super) fn token_decimals() -> u8 {
			<Metadata<T>>::get().decimals
		}

		/// Returns the amount of tokens owned by the sender (`origin`) account.
		pub(super) fn balance_of(origin: OriginFor<T>) -> Result<T::Balance, DispatchError> {
			let dest = ensure_signed(origin)?;

			Ok(Self::read_balance_of(&dest))
		}

		/// Returns the remaining number of tokens that spender (`dest`)  will be
		/// allowed to spend on behalf of the owner (`origin`) through `transfer_from`.
		/// This value changes when `approve` or `transfer_from` are called.
		///
		/// The `origin` must be signed.
		///
		/// - `origin`: The account of the owner.
		/// - `dest`: The account tof the spender allowed to spend on behalf of the owner.
		pub(super) fn allowance(
			origin: OriginFor<T>,
			dest: <T::Lookup as StaticLookup>::Source,
		) -> Result<T::Balance, DispatchError> {
			let owner = ensure_signed(origin)?;
			let spender = T::Lookup::lookup(dest)?;

			Ok(Self::read_allowance_of(&owner, &spender))
		}

		/// Sets `amount` as the allowance of spender (`dest`) over the caller's tokens.
		///
		/// The `origin` must be signed.
		///
		/// If `amount` is the maximum `Balance` value, the allowance is not updated on
		/// `transfer_from`. This is semantically equivalent to an infinite approval.
		///
		///
		/// - `origin`: The account of the owner.
		/// - `dest`: The account of the spender allowed to spend on behalf of the owner.
		/// - `amount`: The number of tokens the spender is allowed to spend on behalf of the owner.
		pub(super) fn approve(
			origin: OriginFor<T>,
			dest: <T::Lookup as StaticLookup>::Source,
			amount: T::Balance,
		) -> DispatchResult {
			let owner = ensure_signed(origin)?;
			let spender = T::Lookup::lookup(dest)?;
			Self::do_approve(&owner, &spender, amount)?;

			Ok(())
		}

		/// Sets balance of an account from a privileged origin.
		///
		/// The `origin` must conform to `ForceOrigin`.
		///
		/// - `dest`: The account of the balance owner.
		///	- `balance`: The number of tokens the balance of an account is set to.
		///
		/// Emits `Transfer` with the amount transferred from `None`.
		pub(super) fn force_set_balance(
			origin: OriginFor<T>,
			dest: <T::Lookup as StaticLookup>::Source,
			balance: T::Balance,
		) -> DispatchResult {
			T::ForceOrigin::ensure_origin(origin)?;
			let id = T::Lookup::lookup(dest)?;
			Self::set_balance(&id, balance)?;
			Self::deposit_event(Event::<T>::Transfer {
				from: None,
				to: id.clone(),
				value: balance,
			});

			Ok(())
		}

		/// Sets `amount` as the allowance of spender (`dest`) over the owner's (`source`) tokens
		/// from a privileged origin.
		///
		/// The `origin` must conform to `ForceOrigin`.
		///
		///
		/// - `source`: The account of the owner.
		/// - `dest`: The account of the spender allowed to spend on behalf of the owner.
		/// - `amount`: The number of tokens the spender is allowed to spend on behalf of the owner.
		pub(super) fn force_set_allowance(
			origin: OriginFor<T>,
			source: <T::Lookup as StaticLookup>::Source,
			dest: <T::Lookup as StaticLookup>::Source,
			amount: T::Balance,
		) -> DispatchResult {
			T::ForceOrigin::ensure_origin(origin)?;
			let owner = T::Lookup::lookup(source)?;
			let spender = T::Lookup::lookup(dest)?;
			Self::set_allowance(&owner, &spender, amount);

			Ok(())
		}

		/// Creates metadata for the token from a privileged origin.
		///
		/// The `origin` must conform to `ForceOrigin`.
		///
		/// - `name`: the name of the token
		/// - `symbol`: the symbol of the token
		/// - `decimals`: The number of decimals the token uses to represent one unit.
		pub(super) fn force_create_metadata(
			origin: OriginFor<T>,
			name: Vec<u8>,
			symbol: Vec<u8>,
			decimals: u8,
		) -> DispatchResult {
			T::ForceOrigin::ensure_origin(origin)?;
			let bounded_name: BoundedVec<_, _> =
				name.clone().try_into().map_err(|()| Error::<T>::StringTooLong)?;
			let bounded_symbol: BoundedVec<_, _> =
				symbol.clone().try_into().map_err(|()| Error::<T>::StringTooLong)?;

			let metadata = TokenMetadata { name: bounded_name, symbol: bounded_symbol, decimals };
			<Metadata<T>>::set(metadata);

			Ok(())
		}

		/// Updates owner's (`from`) allowance for spender (`to`) based on spent `amount`.
		/// Sets allowance to remaining value (i.e. reduces the current allowance value by `amount`).
		///
		/// - `from`: The account of the owner.
		/// - `to`: The account of the spender allowed to spend on behalf of the owner.
		/// - `amount`: The number of tokens the allowance is reduced by.
		fn spend_allowance(
			from: &T::AccountId,
			to: &T::AccountId,
			amount: T::Balance,
		) -> DispatchResult {
			let current = Self::read_allowance_of(&from.clone(), &to.clone());
			if current == T::Balance::max_value() {
				return Ok(());
			}
			ensure!(current >= amount, Error::<T>::InsufficientAllowance);
			Self::do_approve(
				&from,
				&to,
				current.checked_sub(&amount).ok_or(ArithmeticError::Underflow)?,
			);

			Ok(())
		}

		fn do_transfer_from_to(
			from: &T::AccountId,
			to: &T::AccountId,
			amount: T::Balance,
		) -> DispatchResult {
			let from_balance = Self::read_balance_of(&from);
			let to_balance = Self::read_balance_of(&to);
			ensure!(from_balance >= amount, Error::<T>::BalanceLow);
			ensure!(from != to, Error::<T>::InvalidTransactor);

			let debit = Self::prep_debit(from_balance, amount)?;
			let credit = Self::prep_credit(to_balance, amount)?;
			Self::set_balance(&from.clone(), debit);
			Self::set_balance(&to.clone(), credit);

			Self::deposit_event(Event::Transfer {
				from: Some(from.clone()),
				to: to.clone(),
				value: amount,
			});

			Ok(())
		}

		fn do_approve(
			from: &T::AccountId,
			to: &T::AccountId,
			amount: T::Balance,
		) -> DispatchResult {
			Self::set_allowance(&from, &to, amount);
			Self::deposit_event(Event::Approval {
				owner: from.clone(),
				spender: to.clone(),
				value: amount,
			});

			Ok(())
		}

		fn prep_credit(
			dest: T::Balance,
			amount: T::Balance,
		) -> Result<T::Balance, ArithmeticError> {
			// Other checks, prep ops can be performed here as well
			dest.checked_add(&amount).ok_or(ArithmeticError::Overflow)
		}

		fn prep_debit(dest: T::Balance, amount: T::Balance) -> Result<T::Balance, ArithmeticError> {
			// Other checks, prep ops can be performed here as well
			dest.checked_sub(&amount).ok_or(ArithmeticError::Underflow)
		}

		fn set_balance(dest: &T::AccountId, balance: T::Balance) -> DispatchResult {
			<Balances<T>>::insert(&dest, balance);
			Ok(())
		}

		fn read_balance_of(dest: &T::AccountId) -> T::Balance {
			<Balances<T>>::get(&dest).unwrap_or(Zero::zero())
		}

		fn read_allowance_of(owner: &T::AccountId, spender: &T::AccountId) -> T::Balance {
			if owner == spender {
				return T::Balance::max_value();
			}
			<Allowances<T>>::get(&owner, &spender).unwrap_or(Zero::zero())
		}

		fn set_total_supply(amount: T::Balance) {
			<TotalSupply<T>>::put(amount);
		}

		fn set_allowance(owner: &T::AccountId, spender: &T::AccountId, amount: T::Balance) {
			<Allowances<T>>::insert(&owner, &spender, &amount);
		}
	}
}
